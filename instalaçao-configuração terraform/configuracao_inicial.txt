terraform init - primeiro comoando a ser executado. Vai ler seu arquivo.tf, vai baixar os plugins e as extensões necessárias
para executar a receita.

C:\gitlab\terraform>terraform init  

Initializing the backend...

Initializing provider plugins...

Terraform has been successfully initialized!

You may now begin working with Terraform. Try running "terraform plan" to see
any changes that are required for your infrastructure. All Terraform commands
should now work.

If you ever set or change modules or backend configuration for Terraform,
rerun this command to reinitialize your working directory. If you forget, other
commands will detect it and remind you to do so if necessary.


terraform plan - gerar um planejamento de como será implantado o que está na receita, seja para criar ou
apagar algum recurso. Irá te mostrar as alterações que foram feitas.


C:\gitlab\terraform>terraform plan

No changes. Your infrastructure matches the configuration.

Terraform has compared your real infrastructure against your configuration and found no differences, so no changes are
needed.

terraform apply - pegará o seu plano de mudança e aplicará. Após o terraform plan te mostrar as auterações a
serem feitas, você aprovando, rode o terraform apply.  Quando vc dá o terraform apply, o terraform guardará o
estado da minha estrutur nessa pasta. É importante guarda o conteúdo dessa pasta no git, para não se perder
esses arquivos.

terraform validate - valida a sintaxe do arquivo .tf



C:\gitlab\terraform>terraform validate
╷
│ Error: Invalid reference
│
│   on receita02.tf line 18, in resource "aws_instance" "instancia01":
│   18:   ami           = ami-02af65b2d1ebdfafc
│
│ A reference to a resource type must be followed by at least one attribute access, specifying the resource 
  name.
╵

C:\gitlab\terraform>terraform validate
Success! The configuration is valid.

terraform fmt 
Acerta a formatação dos arquivos .tf no diretório onde está sendo executado, e alinha os iguais "="

terraform plan -out="tfplan.out"
O terraform escreve nessa arquivo o resultado do "terraform plan". Qaundo for executado o "terraform 
apply", podemos utilizar esse arquivo

Quando aparece "update in-place" na execução do "terraform plan", é que, ao executar o "terraform apply"
será feita uma atualização.

C:\gitlab\terraform>terraform apply

No changes. Your infrastructure matches the configuration.

terraform apply -auto-approve - não solicitará confirmação, executará o apply sem configmação.

terraform destroy - destruirá toda a infraestrutura criada pelo terraform. Tomará como base o arquivo 
terraform.tfstate

C:\gitlab\terraform>terraform destroy
aws_instance.web: Refreshing state... [id=i-0d6f05490814187af]

Terraform used the selected providers to generate the following execution plan. Resource actions are indicated with the following symbols:
  - destroy

Terraform will perform the following actions:

  # aws_instance.web will be destroyed
  - resource "aws_instance" "web" {
      - ami                                  = "ami-0c02fb55956c7d316" -> null
      - arn                                  = "arn:aws:ec2:us-east-1:711412357492:instance/i-0d6f05490814187af" -> null
      - associate_public_ip_address          = true -> null
      - availability_zone                    = "us-east-1b" -> null
      - cpu_core_count                       = 1 -> null
      - cpu_threads_per_core                 = 1 -> null
      - disable_api_termination              = false -> null
      - ebs_optimized                        = false -> null
      - get_password_data                    = false -> null
      .
      .
      .
      
      - root_block_device {
          - delete_on_termination = true -> null
          - device_name           = "/dev/xvda" -> null
          - encrypted             = false -> null
          - iops                  = 100 -> null
          - tags                  = {} -> null
          - throughput            = 0 -> null
          - volume_id             = "vol-00f64f0913878d679" -> null
          - volume_size           = 8 -> null
          - volume_type           = "gp2" -> null
        }
    }

Plan: 0 to add, 0 to change, 1 to destroy.

Do you really want to destroy all resources?
  Terraform will destroy all your managed infrastructure, as shown above.
  There is no undo. Only 'yes' will be accepted to confirm.

  Enter a value: yes

aws_instance.web: Destroying... [id=i-0d6f05490814187af]
aws_instance.web: Still destroying... [id=i-0d6f05490814187af, 10s elapsed]
aws_instance.web: Still destroying... [id=i-0d6f05490814187af, 20s elapsed]
aws_instance.web: Still destroying... [id=i-0d6f05490814187af, 30s elapsed]
aws_instance.web: Still destroying... [id=i-0d6f05490814187af, 40s elapsed]
aws_instance.web: Destruction complete after 41s

Destroy complete! Resources: 1 destroyed.
-x-x-x-x-x-x
Variáveis 
Pode-se configurar as variáveis num arquivo separado do arquivo principal.
Ex.:
Criar um arquivo chamado vars.tf e colocar os comandos abaixo:

variable "aws_region" {
  type        = string
  description = ""
  default     = "us-east-1"
}

No arquivo principal (main.tf), substituímos o valor default da região por var.aws_region,
onde "var" indica que é uma variável e "aws_region" o nome da variável.

provider "aws" {
  region  = var.aws_region
}

Uma outra forma de setar variáveis no terraform, é utilizando arquivos com a extensão .tfvar
Arquivos com extensão .tfvar é específico para setar variáveis no terracom.
Ex.:
Criar um arquivo chamado terraform.tfvars, por exemplo. A sintaxe é <nome variável> = <Valor variável>

aws_profile = "default"  # está sendo setado o valor "default" a variável "aws_profile"
aws_region = "us-east-1"
instance_ami = "ami-0c02fb55956c7d316"
instance_type = "t2.micro"

Pode-se criar arquivos específicos para se rodar o terraform setando variáveis diferentes, por exemplo
um ambiente de desenvolvimento e um ambiente de produção.

Deve-se criar um arquivo dev.auto.tfvars, e colocar os comandos abaixo:

envinroment = "dev" # variável de ambiente setado como dev

Ao rodar o terraform plan, não será solicitado nada, pois o terraform lê automaticamente os arquivos
com estensão .tfvar

Podemos criar um segundo arquivo para produção, com o nome prod.tfvars:

instance_type = "t2.micro"
envinroment = "prod"
  default = {
    Name    = "Ubuntu"
    Project = "Curso AWS com Terraform"
  }

Para executar o terraform plan, utilize a sintaxe abaixo:

terraform plan -var-file="prod.tfvars" # será executado o terraform paln, conforme variáveis que estão
no arquivo prod.tfvars

Para o apply:
terraform apply -var-file="prod.tfvars" -auto-approve

Para o destroy:
terraform destroy -var-file="prod.tfvars" -auto-approve

Ordem da execução das variáveis:
1 - variáveis de ambiente
2 - o arquivo terraform.tfvars
3 - o arquivo terraform.tfvars.json
4 - qualquer arquivo *.auto.tfvars ou *.auto.tfvars.jason
5 - qualquer opção -var ou -var-file na linha de comando


-x-x-x-x-
TF_VAR_aws_profile=default terraform plan - executará o terraform setando a variável de ambiente TF_VAR_aws_profile.

terraform plan -var="aws_profile=default" -var="instance_type=t2.micro"
Executa o terraform plan passando as variáveis manualmente 

-x-x-x-x-x-
Interpolação
Documentação oficial:
https://www.terraform.io/language/expressions/strings#interpolation

Uma sequência ${ ... } é uma interpolação, que avalia a expressão dada entre os marcadores, converte o resultado em uma string, 
se necessário, e a insere na string final:

Concatenar strings com alguma expressão, seja ela uma variável ou o retorno de uma função
Ex.:
resource "aws_s3_bucket" "this" {
  bucket = "${random_pet.bucket.id}-${var.environment}"
}

- ${random_pet.bucket.id} - interpolação do do id do nome randomico que será criado pelo provider random_pet para o bucket

Concatenando com a interpolação ${var.environment}

Local Values
Documentação oficial:
https://www.terraform.io/language/values/locals

Um valor local atribui um nome a uma expressão, para que você possa usá-lo várias vezes em um módulo sem repeti-lo.

Variáveis de entrada são como argumentos de função.

Os valores de saída são como os valores de retorno da função.

Os valores locais são como variáveis locais temporárias de uma função.

Nota: Por questões de brevidade, os valores locais são muitas vezes referidos apenas como "locais" quando o significado
 é claro a partir do contexto.

Output Values
Documentação oficial:
https://www.terraform.io/language/values/outputs

Os valores de saída disponibilizam informações sobre sua infraestrutura na linha de comando e podem expor informações
para outras configurações do Terraform usarem. Os valores de saída são semelhantes aos valores de retorno em linguagens
de programação.

Os valores de saída têm vários usos:

Um módulo filho pode usar saídas para expor um subconjunto de seus atributos de recursos a um módulo pai.

Um módulo raiz pode usar saídas para imprimir determinados valores na saída da CLI após executar o terraform apply.

Ao usar o estado remoto, as saídas do módulo raiz podem ser acessadas por outras configurações por meio de uma fonte
de dados terraform_remote_state.

Instâncias de recursos gerenciadas pelo Terraform cada exportam atributos cujos valores podem ser usados em outro 
lugar na configuração. Os valores de saída são uma maneira de expor algumas dessas informações ao usuário do seu módulo.


Recursos:
random_pet - gera um nome aleatório a ser criado em outros recursos
Documentação oficial:
https://registry.terraform.io/providers/hashicorp/random/latest/docs/resources/pet




-x-x-x-x-x-x
Dependências Implicitas

Quanbdo uma recurso depende de outro recurso, fazendo um vínculo direto.

resource "aws_instance" "instancia01" {
  ami           = "ami-04505e74c0741db8d"
  instance_type = "t2.micro"
}

resource "aws_eip" "eipinstancia01" {
  instance = aws_instance.instancia01.id
  vpc      = true
}

No exemnplo ascima, o recurso elastic IP só será criado após a criação da instancia "instancia01", pois a criação
do Elastic IP depende do fornecimento do ID da instancia, que será fornecido na linha
 "instance = aws_instance.instancia01.id" (retornará o ID da instância criada ascima - instancia01).

-x-x-x-x-x-
Dependência Explicita

É definido um parâmetro "depends_on" para definir que esse recurso para ser criado depende que outro recurso
já esteja criado.

resource "aws_instance" "instancia01" {
  ami           = "ami-04505e74c0741db8d"
  instance_type = "t2.micro"
  depends_on = [aws_s3_bucket.bucket01]
}

No exe,plo ascima, o recurso "aws_instance" que criará a instancia "instancia01" depende da criação de um
bucket S3 com o nome "bucket01"

-x-x-x-x-
Recursos não Dependentes

Nas boas práticas do terraform, não se deve utilizar a dependência explicitao "depends_on" a todo momento, pois o terraform criar vários Recursos
ao mesmo tempo. Com a dependência explicita, o terraform não irá criar os recursos ao mesmo tempo, pois um recurso depende do outro.

-x-x-x-x-x-x-
Provisionadores
A terraform aconselha a não usar esses comando do terraform. Ele aconselha a utilizar os recursos do provider

-x-x-x-x-x-x-
toda vez que for colocado um novo módulo no seu arquivo .tf, é importante executar o comando "terraform init",
pois com isso o teraform irá baixar esse módulo.

