terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 3.0"
    }
  }
}

# Configure the AWS Provider
provider "aws" {
  region     = var.aws_region # variavel que está definida no arquivo vars.tf
  access_key = var.access_key # variavel que está definida no arquivo vars.tf
  secret_key = var.secret_key # variavel que está definida no arquivo vars.tf
}

data "aws_ami" "ubuntu" {
  most_recent = true

  filter {
    name   = "name"
    values = ["ubuntu/images/hvm-ssd/ubuntu-focal-20.04-amd64-server-*"]
  }

  filter {
    name   = "virtualization-type"
    values = ["hvm"]
  }

  owners = ["099720109477"] # Canonical
}

resource "aws_instance" "instancia01" {
  ami           = data.aws_ami.ubuntu.id
  instance_type = var.instance_type
 # provisioner "local-exec" {
 #   command = "echo ${aws_instance.instancia01[*].private_ip}>> private_ips.txt"
 # }

  lifecycle {
    ignore_changes = [
      # Ignore changes to tags, e.g. because a management agent
      # updates these based on some ruleset managed elsewhere.
      tags,
    ]
  }

count = 2 # Seão criadas duas instâncias idênticas

  tags = {
    Name = "terracom01"
  }
}


