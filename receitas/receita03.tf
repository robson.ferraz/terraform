terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 3.0"
    }
  }
}

# Configure the AWS Provider
provider "aws" {
  region = var.aws_region     # variavel que está definida no arquivo vars.tf
  access_key = var.access_key # variavel que está definida no arquivo vars.tf
  secret_key = var.secret_key # variavel que está definida no arquivo vars.tf
}

resource "aws_security_group" "security_group_teste" {
  name        = "allow_tls"
  description = "Allow TLS inbound traffic"
  
  ingress {
    description      = "TLS from VPC"
    from_port        = 443
    to_port          = 443
    protocol         = "tcp"
    cidr_blocks      = var.sg_cidrs # variavel que está definida no arquivo vars.tf
     }

  egress {
    from_port        = 0
    to_port          = 0
    protocol         = "-1"
    cidr_blocks      = ["0.0.0.0/0"]
  }

  tags = {
    Name = "allow_tls"
  }
}