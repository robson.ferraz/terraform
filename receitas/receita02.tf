terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 3.0"
    }
  }
}

# Configure the AWS Provider
provider "aws" {
  region = "us-east-1"
  access_key = "AKIAX2ZXH4BIY3XBDKDD"
  secret_key = "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"
}

resource "aws_instance" "instancia01" {
  ami           = "ami-04505e74c0741db8d"
  instance_type = "t2.micro"

  provisioner "local-exec" {
      command = "echo ${aws_instance.instancia01.private_ip}>> private_ips.txt"
  }
}

resource "aws_eip" "eipinstancia01" {
  instance = aws_instance.instancia01.id
  vpc      = true
}

resource "aws_security_group" "security_group_teste" {
  name        = "allow_tls"
  description = "Allow TLS inbound traffic"
  
  ingress {
    description      = "TLS from VPC"
    from_port        = 443
    to_port          = 443
    protocol         = "tcp"
    cidr_blocks      = ["179.93.229.109/32","179.93.229.53/32"]
     }

  egress {
    from_port        = 0
    to_port          = 0
    protocol         = "-1"
    cidr_blocks      = ["0.0.0.0/0"]
  }

  tags = {
    Name = "allow_tls"
  }
}