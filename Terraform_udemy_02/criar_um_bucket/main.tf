terraform {
  required_version = "1.1.5"
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "4.6.0"
    }
  }
}

provider "aws" {
  region = "us-east-1"
}

resource "aws_s3_bucket" "my-test-bucket" {
  bucket = "bucket-teste-rob123456789"

  tags = {
    Name        = "My bucket"
    Environment = "Dev"
    ManagedBy   = "Terraform"
    Owner       = "Robson Ferraz"
    UpdatedAt   = "2022-03-22"
  }
}

resource "aws_s3_bucket_acl" "my_acl" {
  bucket = aws_s3_bucket.my-test-bucket.id
  acl    = "private"
}