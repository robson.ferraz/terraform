locals {
  ip_filepath = "ips.json"

  common_tags = {
    Name        = "Curso Terraform"
    ManagedBy   = "Terraform"
    Environment = var.environment
    Owner       = "Robson Ferraz"
  }
}