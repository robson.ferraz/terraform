output "buckey_name" {
    value = aws_s3_bucket.this.bucket
}

output "buckey_arn" {
    value = aws_s3_bucket.this.arn
    description = ""
}

output "buckey_domain_name" {
    value = aws_s3_bucket.this.bucket_domain_name
}

output "ip_file_path" {
    value = "${aws_s3_bucket.this.bucket}/${aws_s3_bucket_object.this.key}"
}