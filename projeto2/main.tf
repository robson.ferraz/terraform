terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 3.0"
    }
  }
}

# Configure the AWS Provider
provider "aws" {
  region     = var.aws_region     # variavel que está definida no arquivo vars.tf
  access_key = var.access_key # variavel que está definida no arquivo vars.tf
  secret_key = var.secret_key # variavel que está definida no arquivo vars.tf
}

module "vpc" {
  source         = "terraform-aws-modules/vpc/aws"
  version        = "2.21.0"

  name               = var.vpc_name
  cidr               = var.vpc_cidr
  azs                = var.vpc_azs
  private_subnets    = var.vpc_private_subnets
  public_subnets     = var.vpc_public_subnets
  enable_nat_gateway = var.vpc_enable_nat_gateway
  tags               = var.vpc_tags
}

module "ec2_instances" {
  source  = "terraform-aws-modules/ec2-instance/aws"
  version = "2.12.0"

  name                   = "my-ec2-cluster"
  instance_count         = 2
  ami                    = "ami-04505e74c0741db8d"
  instance_type          = "t2.micro"
  vpc_security_group_ids = [module.vpc.default_security_group_id]  # pega o security group do output do modulo "VPC"
  subnet_id              = module.vpc.public_subnets[0] # pega as subnets públicas do output do modulo VPC
  tags = {
    Terraform   = "true"
    Environment = "dev"
  }
}