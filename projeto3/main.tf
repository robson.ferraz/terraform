terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 3.0"
    }
  }
}


# Configure the AWS Provider
provider "aws" {
  region     = var.aws_region     # variavel que está definida no arquivo vars.tf
  access_key = var.access_key # variavel que está definida no arquivo vars.tf
  secret_key = var.secret_key # variavel que está definida no arquivo vars.tf
}

resource "aws_instance" "instancia01" {
  ami           = data.aws_ami.amazon_linux.id # indica qual o datasource será chamado (aws), e o nome lógico do
                                              # datasource (amazon_linux) e fornece o ID da imágem em datasource.rf
  instance_type = "t2.micro"
}
